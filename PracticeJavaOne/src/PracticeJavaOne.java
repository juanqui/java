/**
 * AWT Sample application
 *
 * @author 
 * @version 1.00 18/09/05
 */

public class PracticeJavaOne {
    
    public static void main(String[] args) {
        	Prime primo = new Prime();
            int cantidad = 4;
            int total = cantidad * cantidad;
            int[] arr = new int[cantidad];
            int[] arrTotal = new int[total];
            int index = 0;
            int k = 2;
            PrintArray pa = new PrintArray();
            OrderArray oa = new OrderArray();
            int m = 0;
            
            for (int j = 0; j < 100; j++){   
                if (primo.isPrime(k) && index < arr.length){
                    arr[index] = k;
                    index++;
                }
                k++;
            }
            
            for (int x = 0; x < arr.length; x++){
                for (int y = 0; y < arr.length; y++){
                    if (x != y){
                        arrTotal[m] = arr[x] * arr[y];
                        m++;
                    }
                    
                }
            }
            
            pa.PrintFirstArray(arr);
            arrTotal = oa.OrderTotalArray(arrTotal);
			pa.PrintFirstArray(arrTotal);

            //show numbers 14 15
            for (int p = 0; p < arrTotal.length; p++){
                if (p > 0 &&  arrTotal[p]- arrTotal[p - 1] == 1){
                    System.out.println(arrTotal[p - 1] + ", "+ arrTotal[p]);
                }
            } 
    }
}