
public class Operators {
	public  int compute(char op, int a, int b){
		switch (op) {
			case '+': return a+b;
			case '-': return a-b;
			case '*': return a*b;
			case '/': return a/b;
		}
		return -1;//No error processing
	}
}
