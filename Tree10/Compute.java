
public  class Compute {
	Operation ope = new Operation();
	Operators oper = new Operators();
	public   void compute(Tree myTree){
		if(myTree != null){
			compute(myTree.left);
			compute(myTree.right);
			if(!ope.ifOperation(myTree.value)){
				TreeCalculator.values.push(Integer.parseInt(myTree.value));
			}else{
				int b = (Integer)TreeCalculator.values.pop();
				int a = (Integer)TreeCalculator.values.pop();
				char op = myTree.value.charAt(0);
				int temResult = oper.compute(op, a, b);
				TreeCalculator.values.push(temResult);
			}
		}
	}
}
