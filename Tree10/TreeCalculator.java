/**
 * @(#)TreeCalculator.java
 *
 *
 * @author 
 * @version 1.00 2018/9/16
 */
import java.util.*;

public class TreeCalculator {
	
	static Stack values = new Stack();

    public static void main(String[] args) {
       
       Inputs imp = new Inputs(); 
       Compute comp = new Compute();
       Show show = new Show();
       
       comp.compute(imp.enterDatas());
       show.result();

    }
}
