/**
 * @(#)Lienzo.java
 *
 *
 * @author 
 * @version 1.00 2018/9/17
 */
import javax.swing.JPanel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JLabel;

 
public class Lienzo extends JPanel implements  KeyListener{
	private JLabel prueba = new JLabel("hola mundo");

	public Lienzo(){
		addKeyListener(this);
		setFocusable(true);
		add(prueba);
	}

    @Override
    public void keyTyped(KeyEvent e){
    	if(e.getKeyChar() == 'w'|| e.getKeyChar() == 'W' || e.getExtendedKeyCode() == KeyEvent.VK_UP){
    		prueba.setLocation(prueba.getX(), prueba.getY() - 5);
    	}
    	if(e.getKeyChar() == 's'|| e.getKeyChar() == 'S' || e.getExtendedKeyCode() == KeyEvent.VK_DOWN){
    		prueba.setLocation(prueba.getX(), prueba.getY() + 5);
    	}
    	if(e.getKeyChar() == 'a'|| e.getKeyChar() == 'A' || e.getExtendedKeyCode() == KeyEvent.VK_LEFT){
    		prueba.setLocation(prueba.getX() - 5, prueba.getY() );
    	} 
    	if(e.getKeyChar() == 'd'|| e.getKeyChar() == 'd' || e.getExtendedKeyCode() == KeyEvent.VK_RIGHT){
    		prueba.setLocation(prueba.getX() + 5, prueba.getY());
    	}
    }
    
    @Override
    public void keyPressed(KeyEvent e){
    }
    
    @Override
    public void keyReleased(KeyEvent e){
    }
    
    
     
}