/**
 * @(#)Square.java
 *
 *
 * @author 
 * @version 1.00 2018/9/18
 */
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Color;

public class Square {
	private static final int DIAMETER = 30;
	int x = 0;
	int y = 0;
	int ya = 0;
	private Game game;

	public Square(Game game,int x, int ya) {   // x position on the top, ya velocity
		this.game= game;
		this.x = x;
		this.ya = ya;
	}

	void move() {
		
		if (Game.SPACEBAR && Game.POWER > 0){
			
		}else if(collision()){
			game.gameOver();
		}
		if(y >= Game.YSIZE){
			y = 0;
		}else{
			y = y + ya;
		}
	}

	private boolean collision() {
		return game.racquet.getBounds().intersects(getBounds());
	}

	public void paint(Graphics2D g) {	
		g.fillRect(x, y, DIAMETER, DIAMETER);
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, DIAMETER, DIAMETER);
	}
}