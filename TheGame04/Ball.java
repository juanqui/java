/**
 * @(#)Ball.java
 *
 *
 * @author 
 * @version 1.00 2018/9/18
 */
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Color;

public class Ball {
	private static final int DIAMETER = 30;
	int x = 0;
	int y = 0;
	int xa = 1;
	int ya = 1;
	private Game game;

	public Ball(Game game) {
		this.game= game;
	}

	void move() {
		if (x + xa < 0)
			xa = 1;
		if (collision()){
			Game.POWER++;			
		}
		if(x >= Game.XSIZE && y >= Game.YSIZE){
			x = 0;
			y = 0;
		}else{
			x = x + xa;
			y = y + ya;
		}		
	}

	private boolean collision() {
		return game.racquet.getBounds().intersects(getBounds());
	}

	public void paint(Graphics2D g) {
		g.fillOval(x+x, y+y, DIAMETER, DIAMETER);	
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, DIAMETER, DIAMETER);
	}
}