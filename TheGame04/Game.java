 /**
 * @(#)Game.java
 *
 *
 * @author 
 * @version 1.00 2018/9/18
 */
/*
import javax.swing.JPanel;
import javax.swing.JLabel; 
 */
 
import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
/*
 class Lienzo extends JPanel {
	private JLabel prueba = new JLabel( "123");
    public Lienzo() {
    	setFocusable(true);
    	add(prueba);
    	prueba.setLocation(5, 5);
    }
}
*/


//@SuppressWarnings("serial")
public class Game extends JPanel {
	public static  boolean jugar = true;
	public static  int XSIZE = 1000;
	public static  int YSIZE = 700;
	public static int POWER = 0;
	public static boolean SPACEBAR = false;
	Ball ball = new Ball(this);
//	Square square = new Square(this);
	Racquet racquet = new Racquet(this);
	
	Square square = new Square(this, 50, 2);
	Square square2 = new Square(this, 200, 1);
	Square square3 = new Square(this, 300, 2);
	Square square4 = new Square(this, 400, 4);
	Square square5 = new Square(this, 500, 1);
	Square square6 = new Square(this, 600, 2);
	Square square7 = new Square(this, 700, 1);
	Square square8 = new Square(this, 780, 2);
	
	Square[] lsSquare = {
		square, square2, square3, square4,square5,square6,square7,square8
	};
	
	public Game() {
		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
				racquet.keyReleased(e);
			}
			@Override
			public void keyPressed(KeyEvent e) {
				racquet.keyPressed(e);
			}
		});
		setFocusable(true);
	}
	
	private void move() {
		ball.move();
		for(Square sq: lsSquare){
			sq.move();
		}
	//	square.move();
		racquet.move();
	}
	////// aqui la estrellita
	public void paint2(Graphics2D g) {
			g.fillOval(25, 5, 20, 20);	
	}
	/////
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		if(POWER > 0){//estrella
			paint2(g2d);
		}
			
		ball.paint(g2d);
		for(Square sq: lsSquare){
			sq.paint(g2d);
		}
		if (Game.SPACEBAR && Game.POWER > 0){

		}else{
			racquet.paint(g2d);
		}
	}
	
	public void gameOver() {
		JOptionPane.showMessageDialog(this, "Game Over", "Game Over", JOptionPane.YES_NO_OPTION);
		jugar = false;
		System.exit(ABORT);
	}

	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("Play The Game");

		Game game = new Game();
		frame.add(game);
		//frame.add(new Lienzo());
		//frame.add(game);
		//
		frame.setSize(XSIZE, YSIZE);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		while (jugar) {
			game.move();
			game.repaint();
			Thread.sleep(10);
		}
	}
}
