/**
 * @(#)Graficadora.java
 *
 *
 * @author 
 * @version 1.00 2018/9/16
 */
import javax.swing.*;
import java.awt.*;

public class Graficadora {
    public static Plano plano;
    public static Line line;
    
    public static void main(String[] args) {
    	IngresoDatos inDatos = new IngresoDatos();
    	inDatos.datosLine();
    	
        Ventana frame = new Ventana();
        frame.setDefaultCloseOperation(Ventana.EXIT_ON_CLOSE);
        frame.setBounds(300, 100, 600, 600);
        frame.setLayout(new BorderLayout());
        frame.setBackground(Color.black);
        plano = new Plano();

        JButton start = new JButton("Iniciar");
        start.addActionListener(frame);
        start.setText("Iniciar");

        frame.add( plano, BorderLayout.CENTER);
        frame.add(start, BorderLayout.SOUTH);

        frame.setVisible( true );
    }
}
