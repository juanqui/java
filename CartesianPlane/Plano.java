import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.*;
import java.awt.*;

public  class Plano extends JPanel{
	
    public Plano() {
        init();
        setBackground(Color.BLACK);
        setOpaque(true);
    }
    
    public void init() {
        this.setBorder(BorderFactory.createLineBorder(new Color(0,0,0,0)));
    }
    
    @Override
     public void paintComponent(Graphics g) {
                    super.paintComponent(g);
                    g.setColor(Color.red);
                    g.drawLine(0, this.getHeight()/2, this.getWidth(), this.getHeight()/2);
                    g.drawLine(this.getWidth()/2, 0,this.getWidth()/2 , this.getHeight());
     }
     
     public void paintLine(Graphics g,int  x1, int y1,int x2,int y2 ){
     		 g.setColor(Color.orange);
             g.drawLine((int)coord_x(x1), (int)coord_y(y1), (int)coord_x(x2), (int)coord_y(y2));
     }
     
     private double coord_x(double x){
         double real_x = x+this.getWidth()/2;
        return real_x;
     }
     
     private double coord_y(double y){
          double real_y = -y+this.getHeight()/2;
          return (real_y);
     }
}
