package com.yourname.Repository;

import com.yourname.Entity.Interviewer;
import org.hibernate.usertype.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InterviewerRepository extends JpaRepository<Interviewer, Long> {

    @Query("select interviewer from Interviewer interviewer where interviewer.id=:id")
    Interviewer findInterviewerById(@Param("id") int id);

    @Query("select interviewer from Interviewer interviewer")
    List<Interviewer> findAllInterviewers();
}