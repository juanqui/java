package com.yourname.Repository;

import com.yourname.Entity.Candidate;
import com.yourname.Service.CandidateService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    @Query("select candidate from Candidate candidate where candidate.id=:id")
    Candidate findCandidateById(@Param("id") int id);
   //@Query("SELECT e
    //FROM Employee e")
    @Query("SELECT c FROM Candidate c" +   "")
    List<Candidate> findAllCandidates();
}
