package com.yourname.Entity;


import javax.persistence.*;

@Entity
@Table(name = "Estudiante")
public class Student {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "empleadoid", referencedColumnName = "eid", nullable = false)
    private Employee employee;


    private String name;
    private String course;

    public Student() {}

    public Student(int id, String name, String course){
        this.id = id;
        this.name = name;
        this.course = course;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}

