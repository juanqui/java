package com.yourname.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Entrevista")
public class Interview {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    private String areaExpertice;
    private Date dateInterview;
    private String observation;
    private int experienceLevel;

    //id Interviewer
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "Interviewer", referencedColumnName = "id", nullable = false)
    private Interviewer interviewer;
    //id Candidate
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "Candidate", referencedColumnName = "id", nullable = false)
    private Candidate candidate;

    public Interview(int id, String areaExpertice, Date dateInterview, String observation, int experienceLevel) {
        this.id = id;
        this.areaExpertice = areaExpertice;
        this.dateInterview = dateInterview;
        this.observation = observation;
        this.experienceLevel = experienceLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAreaExpertice() {
        return areaExpertice;
    }

    public void setAreaExpertice(String areaExpertice) {
        this.areaExpertice = areaExpertice;
    }

    public Date getDateInterview() {
        return dateInterview;
    }

    public void setDateInterview(Date dateInterview) {
        this.dateInterview = dateInterview;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public int getExperienceLevel() {
        return experienceLevel;
    }

    public void setExperienceLevel(int experienceLevel) {
        this.experienceLevel = experienceLevel;
    }
}
