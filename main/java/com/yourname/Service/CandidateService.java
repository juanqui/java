package com.yourname.Service;

import com.yourname.Entity.Candidate;
import com.yourname.Repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CandidateService {
    @Autowired
    public CandidateRepository candidateRepository;

    public Candidate createCandidate(Candidate instance) {
        return candidateRepository.save(instance);
    }

    public Candidate findCandidateById(int id) {
        return candidateRepository.findCandidateById(id);
    }

    public List<Candidate> getAllCandidates() {
        return candidateRepository.findAllCandidates();
    }

    public void deleteCandidate(Candidate instance) { candidateRepository.delete(instance);  }
}
