package com.yourname.Controller;

import com.yourname.Entity.Interview;

import com.yourname.Service.InteviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/interview")
public class InterviewController {
    @Autowired
    private InteviewService inteviewService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Interview> getAllInterviews() { return inteviewService.getAllInterviews();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Interview getInterviewById(@PathVariable("id") int id){
        return inteviewService.findInterviewById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteInterviewById(@PathVariable("id") int id){
        Interview instance = getInterviewById(id);
        inteviewService.deleteInterview(instance);
    }
}
