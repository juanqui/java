/**
 * @(#)Game.java
 *
 *
 * @author 
 * @version 1.00 2018/9/18
 */
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

//@SuppressWarnings("serial")
public class Game extends JPanel {
	public static  boolean jugar = true;
	Ball ball = new Ball(this);
//	Square square = new Square(this);
	Racquet racquet = new Racquet(this);
	
	Square square = new Square(this, 50, 2);
	Square square2 = new Square(this, 200, 1);
	Square square3 = new Square(this, 300, 2);
	
	Square[] lsSquare = {
		square,
		square2,
		square3
	};
	
	
	public Game() {
		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				racquet.keyReleased(e);
			}

			@Override
			public void keyPressed(KeyEvent e) {
				racquet.keyPressed(e);
			}
		});
		setFocusable(true);
	}
	
	private void move() {
		ball.move();
		for(Square sq: lsSquare){
			sq.move();
		}
	//	square.move();
		racquet.move();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
				
		ball.paint(g2d);
		for(Square sq: lsSquare){
			sq.paint(g2d);
		}
	//	square.paint(g2d);
		racquet.paint(g2d);
	}
	
	public void gameOver() {
		JOptionPane.showMessageDialog(this, "Game Over", "Game Over", JOptionPane.YES_NO_OPTION);
		jugar = false;
		System.exit(ABORT);
	}

	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("Play the Game");
		Game game = new Game();
		frame.add(game);
		frame.setSize(500, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		while (jugar) {
			game.move();
			game.repaint();
			Thread.sleep(10);
		}
	}
}
