/**
 * @(#)Racquet.java
 *
 *
 * @author 
 * @version 1.00 2018/9/18
 */
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

public class Racquet {
	private static final int Y = 330;
	private static final int WITH = 50;
	private static final int HEIGHT = 40;
	private static final int INCREMENT = 3;
	int x = 200;
	int y = 300;
	int xa = 0;
	int ya = 0;
	private Game game;

	public Racquet(Game game) {
		this.game = game;
	}

	public void move() {
		if (x + xa > 0 && x + xa < game.getWidth() - WITH)
			x = x + xa;
		if (y + ya > 0 && y + ya < game.getHeight() - HEIGHT)
			y = y + ya;
	}

	public void paint(Graphics2D g) {
		g.fillRect(x, y, WITH, HEIGHT);
	}

	public void keyReleased(KeyEvent e) {
		xa = 0;
		ya = 0;
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
			xa = -INCREMENT;
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			xa = INCREMENT;
		if (e.getKeyCode() == KeyEvent.VK_UP)
			ya = -INCREMENT;
		if (e.getKeyCode() == KeyEvent.VK_DOWN)
			ya = INCREMENT;
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, WITH, HEIGHT);
	}

	public int getTopY() {
		return y - HEIGHT;
	}
}
