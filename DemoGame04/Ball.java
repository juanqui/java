/**
 * @(#)Ball.java
 *
 *
 * @author 
 * @version 1.00 2018/9/18
 */
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Ball {
	private static final int DIAMETER = 30;
//	private static final int WITH = 40;
//	private static final int HEIGHT = 40;
	int x = 0;
	int y = 0;
	int xa = 1;
	int ya = 1;
	private Game game;

	public Ball(Game game) {
		this.game= game;
	}

	void move() {
		if (x + xa < 0)
			xa = 1;
				
			
	/*	if (x + xa > game.getWidth() - DIAMETER)
			xa = -1;
		if (y + ya < 0)
			ya = 1;
		if (y + ya > game.getHeight() - DIAMETER)
	*/	//	game.gameOver();
		if (collision()){
		//	ya = -1;
		//	y = game.racquet.getTopY() - DIAMETER;
			game.gameOver();
		}
		if(x >= 601 && y >= 601){
			x = 0;
			y = 0;
		}else{
			x = x + xa;
			y = y + ya;
		}
		
	}

	private boolean collision() {
		return game.racquet.getBounds().intersects(getBounds());
	}

	public void paint(Graphics2D g) {
	//	g.fillOval(x+x, y+y, DIAMETER, DIAMETER);
	//	g.fillRect(x+x, y+y, DIAMETER, DIAMETER);
		
		g.fillRect(x, y, DIAMETER, DIAMETER);
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, DIAMETER, DIAMETER);
	}
}