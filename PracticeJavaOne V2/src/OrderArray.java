/**
 * @(#)PrintArray.java
 *
 *
 * @author 
 * @version 1.00 2018/9/5
 */


public class OrderArray {

    public OrderArray() {
    }
    public int[] OrderTotalArray(int[] arrTotal){
    		int aux;
    		for (int x = 0; x < arrTotal.length; x++) {
		        for (int i = 0; i < arrTotal.length-x-1; i++) {
		            if(arrTotal[i] < arrTotal[i+1]){
		                int tmp = arrTotal[i+1];
		                arrTotal[i+1] = arrTotal[i];
		                arrTotal[i] = tmp;
		            }
		        }
		    }
		    
		    for (int i=0; i<arrTotal.length/2; i++){
	            aux = arrTotal[i];
	            arrTotal[i] = arrTotal[arrTotal.length-1-i];
	            arrTotal[arrTotal.length-1-i] = aux;
	        }
	    	return arrTotal;
    }
    
}

