package com.yourname.Repository;

import com.yourname.Entity.Interview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InterviewRepository extends JpaRepository<Interview, Long> {

    @Query("select interview from Interview interview where interview.candidate.id=:id")
    Interview findInterviewById(@Param("id") int id);

    @Query("select interview from Interview  interview")
    List<Interview> finaAllInterviews();
}