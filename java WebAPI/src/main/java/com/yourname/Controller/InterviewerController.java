package com.yourname.Controller;


import com.yourname.Entity.Interviewer;
import com.yourname.Service.InterviewerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/interviewer")
public class InterviewerController {
    @Autowired
    private InterviewerService interviewerService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Interviewer> getAllInterviewers() { return interviewerService.getAllInterviewers();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Interviewer getInterviewerById(@PathVariable("id") int id){
        return interviewerService.findInterviewerById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteInterviewerById(@PathVariable("id") int id){
        Interviewer instance = getInterviewerById(id);
        interviewerService.deleteInterviewer(instance);
    }
}
