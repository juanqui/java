package com.yourname.Controller;

import com.yourname.Entity.Candidate;
//import com.yourname.Entity.Student;
import com.yourname.Service.CandidateService;
//import com.yourname.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/candidate")
public class CandidateController {
    @Autowired
    private CandidateService candidateService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Candidate> getAllCandidates() {
        return candidateService.getAllCandidates();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Candidate getCandidateById(@PathVariable("id") int id){
        return candidateService.findCandidateById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteCandidateById(@PathVariable("id") int id){
        Candidate instance = getCandidateById(id);
        candidateService.deleteCandidate(instance);
    }
}
