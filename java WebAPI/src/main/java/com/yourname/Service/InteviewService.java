package com.yourname.Service;

import com.yourname.Entity.Interview;
import com.yourname.Repository.InterviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class InteviewService {
    @Autowired
    private InterviewRepository interviewRepository;

    public Interview createInterview(Interview instance) {
        return interviewRepository.save(instance);
    }

    public Interview findInterviewById(int id) {
        return interviewRepository.findInterviewById(id);
    }

    public List<Interview> getAllInterviews() {
        return interviewRepository.finaAllInterviews();
    }

    public void deleteInterview(Interview instance) { interviewRepository.delete(instance);  }
}
