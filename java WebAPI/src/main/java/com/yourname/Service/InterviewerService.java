package com.yourname.Service;

import com.yourname.Entity.Candidate;
import com.yourname.Entity.Interviewer;
import com.yourname.Repository.InterviewerRepository;
import org.hibernate.usertype.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class InterviewerService {
    @Autowired
    private InterviewerRepository interviewerRepository;

    public Interviewer createInterviewer(Interviewer instance) {
        return interviewerRepository.save(instance);
    }

    public Interviewer findInterviewerById(int id) {
        return interviewerRepository.findInterviewerById(id);
    }

    public List<Interviewer> getAllInterviewers() { return interviewerRepository.findAllInterviewers(); }

    public void deleteInterviewer(Interviewer instance) { interviewerRepository.delete(instance);  }
}
